import React from "react"
import Svg, { G, Path, Ellipse } from "react-native-svg"

function SvgComponent(props) {
  return (
    <Svg
      width={102.549}
      height={105.456}
      viewBox="0 0 102.549 105.456"
      {...props}
    >
      <G
        transform="translate(1 1)"
        fill="none"
        stroke={props.fill || "#e25252"}
        strokeLinecap="square"
      >
        <Path
          data-name="Caminho 208"
          d="M100.132 103.042l-26.7-27.478"
          strokeWidth={5}
        />
        <Ellipse
          data-name="Elipse 1"
          cx={42.915}
          cy={45.161}
          rx={38.915}
          ry={38.161}
          strokeMiterlimit={10}
          strokeWidth={5}
        />
      </G>
    </Svg>
  )
}

export default SvgComponent