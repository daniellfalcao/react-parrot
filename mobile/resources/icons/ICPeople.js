import React from "react"
import Svg, { G, Path } from "react-native-svg"

function SvgComponent(props) {
  return (
    <Svg
      width={102.129}
      height={115.339}
      viewBox="0 0 102.129 115.339"
      {...props}
    >
      <G
        fill="none"
        stroke={props.fill || "#db3130"}
        strokeLinecap="square"
        strokeMiterlimit={10}
        strokeWidth={7}
      >
        <Path
          data-name="Caminho 5"
          d="M51.064 57.67h0c-13.823 0-25.032-11.535-25.032-25.759v-5.152C26.032 12.535 37.241 1 51.064 1h0c13.823 0 25.032 11.535 25.032 25.759v5.152c0 14.224-11.209 25.759-25.032 25.759z"
        />
        <Path
          data-name="Caminho 6"
          d="M101.129 103.562A20.473 20.473 0 0086.44 83.697c-9.482-2.664-22.429-5.42-35.376-5.42s-25.893 2.756-35.376 5.42A20.473 20.473 0 001 103.562v10.778h100.129z"
        />
      </G>
    </Svg>
  )
}

export default SvgComponent