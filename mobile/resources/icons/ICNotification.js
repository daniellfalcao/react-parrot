import React from "react"
import Svg, { G, Path } from "react-native-svg"

function SvgComponent(props) {
  return (
    <Svg
      width={105.154}
      height={129.626}
      viewBox="0 0 105.154 129.626"
      {...props}
    >
      <G
        fill="none"
        stroke={props.fill || "#e25252"}
        strokeLinecap="square"
        strokeMiterlimit={10}
      >
        <Path
          data-name="Caminho 3"
          d="M69.489 111.222c0 9.862-7.329 17.4-16.913 17.4s-16.912-7.538-16.912-17.4"
          strokeWidth={5}
        />
        <Path
          data-name="Caminho 4"
          d="M92.039 70.614V41.608C92.039 18.984 74.563 1 52.576 1S13.114 18.984 13.114 41.608v29.006a81.864 81.864 0 01-11.275 40.608h101.475a81.864 81.864 0 01-11.275-40.608z"
          strokeWidth={5}
        />
      </G>
    </Svg>
  )
}

export default SvgComponent