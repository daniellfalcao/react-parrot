import React from "react"
import Svg, { Path } from "react-native-svg"

function SvgComponent(props) {
  return (
    <Svg
      width={100.425}
      height={63.601}
      viewBox="0 0 100.425 63.601"
      {...props}
    >
      <Path
        data-name="Caminho 233"
        d="M0 63.601l100.425-31.8L0 0v24.734L71.732 31.8 0 38.867z"
        fill="#e25252"
      />
    </Svg>
  )
}

export default SvgComponent
