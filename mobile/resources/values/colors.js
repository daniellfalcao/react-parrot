const colors = {
    
    primary: '#AA1A29',
    accent: '#E25252',
    windowBackground: '#FAFAFA',

    textHint: '#BABABA',
    textError: '#AA1A29',
    textPlaceholder: "#B9B9B9",

    bottomBarActive: '#E25252',
    bottomBarInactive: '#B9B9B9',
    bottomBarBackground: '#FFFFFF',
}

export default colors