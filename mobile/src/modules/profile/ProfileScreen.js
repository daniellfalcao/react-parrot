import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    SafeAreaView,
    Text
} from 'react-native';

import colors from '../../../resources/values/colors.js'

export default class ProfileScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render() {
        return (
            <View style={styles.container}>
                <SafeAreaView>
                    <Text> Profile </Text>
                </SafeAreaView>
            </View>
        );
    }
}

const styles = StyleSheet.create({

    container: {
        flex: 1,
        backgroundColor: colors.windowBackground,
    },
})
