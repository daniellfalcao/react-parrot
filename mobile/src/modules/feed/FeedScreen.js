import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    SafeAreaView,
    TouchableOpacity,
    Text,
    TextInput
} from 'react-native';

import PostInput from '../../components/PostInput'

import colors from '../../../resources/values/colors.js'

import ICPostImage from '../../../resources/icons/ICPostImage.svg'
import ICSend from '../../../resources/icons/ICSend.js'

export default class FeedScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {

        };
    }

    render() {
        return (
            <View style={styles.container}>
                <SafeAreaView>
                    <PostInput/>
                </SafeAreaView>
            </View>
        );
    }
}

const styles = StyleSheet.create({

    container: {
        flex: 1,
        backgroundColor: colors.windowBackground,
    },

    containerNewPost: {
        backgroundColor: "#FFF"
    },

    containerInputNewPost: {
        backgroundColor: "#F4F4F4",
        margin: 8,
        padding: 8,
        borderRadius: 8,
        flexDirection: 'column',
    },

    containerOptions: {
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'space-between'
    },

    inputPost: {

    },

    buttonPostImage: {
        marginTop: 8,
        alignSelf: 'flex-start'
    },

    buttonSendPost: {
        alignSelf: 'flex-end'
    },
})
