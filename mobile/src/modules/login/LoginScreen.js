import React, {
    Component,
    useState,
    useEffect
} from 'react'

import {
    processColor,
    StyleSheet,
    StatusBar,
    Platform,
    Keyboard,
    View,
    SafeAreaView,
    KeyboardAvoidingView,
    TouchableWithoutFeedback,
    Text,
    TextInput,
    TouchableOpacity,
    Button
} from 'react-native'

import { StackActions, NavigationActions } from 'react-navigation';

import MaterialTextInput from '../../components/MaterialTextInput'

import ICParrot from '../../../resources/icons/ICParrot.svg'

import colors from '../../../resources/values/colors.js'

export default class Login extends Component {

    constructor(props) {
        super(props)
        this.state = {
            isEmailFocused: false,
            isPasswordFocused: false,
            showCadastro: true
        }
    }

    async componentDidMount() {

        this.onKeyboardShowListener = Keyboard.addListener('keyboardWillShow', this._keyboardDidShow.bind(this));
        this.onKeyboardHideListener = Keyboard.addListener('keyboardWillHide', this._keyboardDidHide.bind(this));

        if (Platform.OS === 'android') {
            StatusBar.setBarStyle('light-content')
            StatusBar.backgroundColor = colors.accent
        } else {
            StatusBar.setBarStyle('dark-content')
        }

        this.handleSubmit()
    }

    componentWillUnmount() {
        this.onKeyboardShowListener.remove();
        this.onKeyboardHideListener.remove();
    }

    _keyboardDidShow() {
        console.log('Keyboard Shown');
        this.setState({ showCadastro: false })
    }

    _keyboardDidHide() {
        console.log('Keyboard Hidden');
        this.setState({ showCadastro: true })
    }

    async handleSubmit() {
        this.props.navigation.dispatch(StackActions.reset({
            index: 0,
            actions: [NavigationActions.navigate({ routeName: 'App' })],
        }))
    }

    async handleRegister() {
        this.props.navigation.navigate('Register')
    }

    render() {
        return (
            <KeyboardAvoidingView style={styles.container} behavior={Platform.OS === "ios" ? "padding" : null}>
                <SafeAreaView style={styles.containerSafeArea}>
                    <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
                        <View style={styles.containerInner}>
                            <View style={styles.containerLogo}>
                                <ICParrot width={8 * 18} color={Platform.OS === 'ios' ? processColor('blue') : 'blue'}/>
                            </View>
                            <View style={styles.containerInput}>
                                <MaterialTextInput
                                    selectionColor={colors.accent}
                                    inputTitle={'Email'}
                                    errorMessage={'Email invalido'}
                                    keyboardType={'email-address'} />
                                <MaterialTextInput
                                    style={{ marginTop: 8 }}
                                    selectionColor={colors.accent}
                                    inputTitle={'Senha'}
                                    errorMessage={'Senha invalida'}
                                    secureTextEntry={true} />
                                <TouchableOpacity style={styles.button} onPress={() => this.handleSubmit()}>
                                    <Text style={styles.buttonText}>Entrar</Text>
                                </TouchableOpacity>
                            </View>
                            <View style={{ flex: 1 }}>
                                <View style={this.state.showCadastro ? styles.containerCadastrar : styles.disable}>
                                    <View style={styles.divider} />
                                    <TouchableOpacity onPress={() => this.handleRegister()}>
                                        <Text style={styles.labelCadastro}>Ainda não tem conta?</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                    </TouchableWithoutFeedback>
                </SafeAreaView>
            </KeyboardAvoidingView >
        )
    }

}

const styles = StyleSheet.create({

    container: {
        flex: 1,
        backgroundColor: colors.windowBackground,
    },

    containerSafeArea: {
        flex: 1,
    },

    containerInner: {
        flex: 1,
        justifyContent: 'space-between',
    },

    containerLogo: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },

    containerInput: {
        flex: 1,
        // backgroundColor: '#32C40F', // verde
        marginStart: 8 * 4,
        marginEnd: 8 * 4,
    },

    containerCadastrar: {
        flex: 1,
        paddingBottom: 8 * 2,
        justifyContent: 'flex-end',
        alignItems: 'center',
    },

    button: {
        height: 8 * 5,
        backgroundColor: colors.accent,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 4,
        marginTop: 8 * 2,
        marginEnd: 8 * 9,
        marginStart: 8 * 9,
    },

    buttonText: {
        color: '#FFF',
        fontWeight: 'bold',
        fontSize: 14
    },

    divider: {
        height: 0.5,
        width: '80%',
        backgroundColor: '#B9B9B9',
    },

    labelCadastro: {
        fontWeight: 'bold',
        marginTop: 8,
        color: '#9E9E9E'
    },

    disable: {
        display: 'none'
    }

})