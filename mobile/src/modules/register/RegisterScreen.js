import React, { Component } from 'react';

import {
	StyleSheet,
	ScrollView,
	View,
	SafeAreaView,
	KeyboardAvoidingView,
	Text,
	StatusBar,
	Image,
	TouchableOpacity,
	Button
} from 'react-native';


import { Dialog, ConfirmDialog } from 'react-native-simple-dialogs'

import MaterialTextInput from '../../components/MaterialTextInput'

import colors from '../../../resources/values/colors.js'

import ICUserPlaceholder from '../../../resources/icons/ICUserPlaceholder.png'
import ICCamera from '../../../resources/icons/ICCamera.svg'
import ICGallery from '../../../resources/icons/ICGallery.svg'

import * as ImagePicker from '../../utils/ImagePicker.js'

export default class RegisterScreen extends Component {

	constructor(props) {
		super(props);
		this.state = {
			isDialogSelectImagePickerVisible: false,
			image: ""
		};
	}

	async _pickImageFromCamera() {
		ImagePicker.getPhotoFromCamera((result) => {
			console.log('setando imagem =>', result.uri)
			this.setState({ image: result.uri })
		})

	}

	async _pickImageFromGallery() {
		ImagePicker.getPhotoFromGalery((result) => {
			console.log('setando imagem =>', result.uri)
			this.setState({ image: result.uri })
		})
	}

	async _showDialogSelectImagePicker() {
		this.setState({ isDialogSelectImagePickerVisible: true })
	}

	async _dismissDialogSelectImagePicker() {
		this.setState({ isDialogSelectImagePickerVisible: false })
	}

	render() {
		return (
			<View>
				<Dialog
					visible={this.state.isDialogSelectImagePickerVisible}
					title="Escolher a foto"
					onTouchOutside={() => this._dismissDialogSelectImagePicker()}>
					<View style={styles.dialogImagePicker}>
						<TouchableOpacity
							style={styles.buttonDialogImagePicker}
							onPress={() => {
								this._dismissDialogSelectImagePicker()
								this._pickImageFromCamera()
							}}>
							<ICCamera width={25} />
							<Text style={styles.buttonDialogImagePickerText}>Câmera</Text>
						</TouchableOpacity>
						<TouchableOpacity
							style={styles.buttonDialogImagePicker}
							onPress={() => {
								this._dismissDialogSelectImagePicker()
								this._pickImageFromGallery()
							}}>
							<ICGallery width={25} />
							<Text style={styles.buttonDialogImagePickerText}>Galeria</Text>
						</TouchableOpacity>
					</View>
				</Dialog>
				<SafeAreaView>
					<StatusBar barStyle="light-content" backgroundColor={colors.primary} />
					<KeyboardAvoidingView style={styles.safeArea}>
						<ScrollView>
							<View style={styles.containerInner}>
								<View style={styles.imageContainer}>
									<Image style={styles.image} source={
										this.state.image
											? { uri: this.state.image }
											: ICUserPlaceholder
									} />
									<TouchableOpacity
										style={styles.buttonCameraContainer}
										onPress={() => this._showDialogSelectImagePicker()}>
										<ICCamera width={15} />
									</TouchableOpacity>
								</View>
								<View style={styles.formContainer}>
									<MaterialTextInput
										selectionColor={colors.accent}
										inputTitle={'Nome Completo *'}
										errorMessage={'Email invalido'}
										keyboardType={'email-address'} />
									<MaterialTextInput
										selectionColor={colors.accent}
										inputTitle={'Username *'}
										errorMessage={'Email invalido'}
										keyboardType={'email-address'} />
									<MaterialTextInput
										selectionColor={colors.accent}
										inputTitle={'Email *'}
										errorMessage={'Email invalido'}
										keyboardType={'email-address'} />
									<MaterialTextInput
										selectionColor={colors.accent}
										inputTitle={'Senha *'}
										errorMessage={'Email invalido'}
										keyboardType={'email-address'} />
									<MaterialTextInput
										selectionColor={colors.accent}
										inputTitle={'Confirmar Senha *'}
										errorMessage={'Email invalido'}
										keyboardType={'email-address'} />
								</View>
								<TouchableOpacity style={styles.buttonFinalizar}>
									<Text style={styles.buttonFinalizarText}>Entrar</Text>
								</TouchableOpacity>
							</View>
						</ScrollView>
					</KeyboardAvoidingView>
				</SafeAreaView>
			</View>
		)
	}
}

const styles = StyleSheet.create({

	container: {
		height: '100%',
		backgroundColor: colors.windowBackground,
	},

	containerInner: {
		flex: 1,
		justifyContent: 'space-between',
	},

	imageContainer: {
		marginTop: 8 * 2,
		alignItems: 'center',
	},

	buttonCameraContainer: {
		position: 'absolute',
		alignItems: 'center',
		justifyContent: 'center',
		backgroundColor: "#FFF",
		width: 8 * 4.5,
		height: 8 * 4.5,
		borderRadius: (8 * 4.5) / 2,
		top: '77%',
		left: '55%',
		elevation: 5,
		padding: 0
	},

	formContainer: {
		marginStart: 8 * 4,
		marginEnd: 8 * 4,
		marginTop: 8 * 4
	},

	inputContainer: {
		marginStart: 8 * 4,
		marginEnd: 8 * 4,
		marginTop: 8 * 2
	},

	image: {
		width: 8 * 15,
		height: 8 * 15,
		borderRadius: (8 * 15) / 2
	},

	buttonFinalizar: {
		height: 8 * 5,
		backgroundColor: colors.accent,
		justifyContent: 'center',
		alignItems: 'center',
		borderRadius: 4,
		marginTop: 8 * 4,
		marginBottom: 8 * 4,
		marginEnd: 8 * 12,
		marginStart: 8 * 12
	},

	buttonFinalizarText: {
		color: '#FFF',
		fontWeight: 'bold',
		fontSize: 14
	},

	dialogImagePicker: {
		// backgroundColor: '#BABABA',
		height: 100,
		flexDirection: 'column',
	},

	buttonDialogImagePicker: {
		flex: 1,
		height: 50,
		marginStart: 8,
		// backgroundColor: '#000',
		flexDirection: 'row',
		alignItems: 'center'
	},

	buttonDialogImagePickerText: {
		marginStart: 8,
		color: '#000',
		fontSize: 14
	},

})

