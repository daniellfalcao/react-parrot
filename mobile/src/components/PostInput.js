import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    SafeAreaView,
    TouchableOpacity,
    Text,
    TextInput,
    Image,
    FlatList
} from 'react-native';

import LinearGradient from 'react-native-linear-gradient';

import colors from '../../resources/values/colors.js'

import ICPostImage from '../../resources/icons/ICPostImage.svg'
import ICSend from '../../resources/icons/ICSend.js'
import ICDeletePostImage from '../../resources/icons/ICDeletePostImage.svg'

export default class PostInput extends Component {

    constructor(props) {
        super(props);
        this.state = {
            images: ["123", "321", "444", "666", "777", "888", "999"],
            hasContent: false
        };

        this.mockImages()
    }

    async mockImages() {
        console.log("criando images")
        console.log(this.state.images)
    }

    async removePhoto(value, index) {
        let newImages = this.state.images.filter((item) => {
            return item !== value 
        })        
        this.setState({ images: newImages})
    }

    render() {
        return (
            <View style={styles.containerNewPost}>
                <View style={styles.containerInputNewPost}>
                    <TextInput
                        style={styles.inputPost}
                        placeholder="O que você está sentindo?"
                        placeholderTextColor={colors.textPlaceholder}
                        multiline={true}>
                    </TextInput>
                    <View style={styles.containerOptions}>
                        <TouchableOpacity style={styles.buttonPostImage}>
                            <ICPostImage width={24} height={24} />
                        </TouchableOpacity>
                        <FlatList
                            style={{ width: '100%', position: 'absolute' }}
                            data={this.state.images}
                            keyExtractor={image => image}
                            horizontal
                            showsHorizontalScrollIndicator={false}
                            renderItem={({ item, index }) => (
                                <View style={[styles.listItem, this.state.images.length - 1 === index ? {marginRight: 64} : { }]}>
                                    <Image style={styles.listItemImage} />
                                    <View style={styles.listItemForeground}>
                                        <TouchableOpacity onPress={() => this.removePhoto(item, index)}>
                                            <ICDeletePostImage style={{ alignSelf: 'center' }} width={24} height={24} />
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            )}
                        />
                        <View style={{ width: 64, height: 64, alignSelf: 'flex-end', position: 'relative' }} >
                            <LinearGradient
                                style={{ width: 8 * 4, height: '100%', justifyContent: 'center', position: 'absolute' }}
                                colors={['#F4F4F4FF', '#F4F4F4FF', '#F4F4F400', '#00000000']}
                                useAngle={true}
                                angle={-90} />
                            <View style={{ backgroundColor: '#f4f4f4', alignSelf: 'flex-end', width: 8 * 4, height: '100%', justifyContent: 'center' }}>
                                <TouchableOpacity backgroundColor={'#f4f4f4'}>
                                    <ICSend
                                        style={{ alignSelf: 'flex-end' }}
                                        width={24}
                                        height={24} />
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                </View>
            </View >
        );
    }
}

const styles = StyleSheet.create({

    container: {
        flex: 1,
        backgroundColor: colors.windowBackground,
    },

    containerNewPost: {
        backgroundColor: "#FFF"
    },

    containerInputNewPost: {
        backgroundColor: "#F4F4F4",
        margin: 8,
        padding: 8,
        borderRadius: 8,
        flexDirection: 'column',
    },

    containerOptions: {
        width: '100%',

        marginTop: 8,
    },

    inputPost: {

    },

    buttonPostImage: {
        display: 'none',
        alignSelf: 'flex-start'
    },

    buttonSendPost: {
        display: 'none',
        alignSelf: 'flex-end'
    },

    listItem: {
        width: 8 * 8,
        height: 8 * 8,
        borderRadius: 8,
        marginRight: 4
    },

    listItemImage: {
        borderRadius: 8,
        width: 8 * 8,
        height: 8 * 8,
        backgroundColor: '#FFF',
        position: 'absolute'
    },

    listItemForeground: {
        width: 8 * 8,
        height: 8 * 8,
        justifyContent: 'center',
        borderRadius: 8,
        backgroundColor: '#E9C3C74D'
    }
})
