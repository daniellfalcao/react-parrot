import React, { Component } from 'react';

import { 
    StyleSheet,
    View, 
    Text, 
    TextInput 
} from 'react-native';

import colors from '../../resources/values/colors.js'

export default class MaterialTextInput extends Component {
    
    constructor(props) {
        super(props);
        this.state = {
            isEmailFocused: false,
            isPasswordFocused: false,
            errorMessage: props.errorMessage
        };
    }

    render() {
        return (
            <View style={this.props.style}>
                <Text style={styles.inputLabel}>{this.props.inputTitle}</Text>
                <TextInput
                    style={this.state.isEmailFocused ? [styles.input, styles.inputFocused] : [styles.input, styles.inputUnfocused]}
                    placeholder={this.props.inputPlaceholder}
                    placeholderTextColor="#999"
                    keyboardType={this.props.keyboardType}
                    autoCapitalize={this.props.autoCapitalize}
                    autoCorrect={this.props.autoCorrect}
                    secureTextEntry={this.props.secureTextEntry}
                    value={this.props.value}
                    onChangeText={this.props.onChangeText}
                    selectionColor={this.props.selectionColor}
                    onFocus={() => this.setState({ isEmailFocused: true })}
                    onBlur={() => this.setState({ isEmailFocused: false })}
                />
                <Text style={styles.inputError}>{this.state.errorMessage}</Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({

    inputLabel: {
        fontWeight: 'bold',
        color: '#E25252',
        marginBottom: 4
    },

    input: {
        borderWidth: 1,
        paddingHorizontal: 8,
        fontSize: 16,
        color: '#444',
        height: 8 * 6,
        borderRadius: 4
    },

    inputFocused: {
        borderColor: colors.accent,
    },

    inputUnfocused: {
        borderColor: '#B9B9B9',
    },

    inputError: {
        marginTop: 4,
        marginBottom: 8,
        marginStart: 0,
        color: colors.textError
    }
})
