import {check, PERMISSIONS, RESULTS, request} from 'react-native-permissions';
import { Platform } from 'react-native'
import ReactImagePicker from 'react-native-image-picker'

import colors from '../../resources/values/colors.js'

export async function getPhotoFromCamera(result) {

    let cameraPermission = Platform.select({
        android: PERMISSIONS.ANDROID.CAMERA,
        ios: PERMISSIONS.IOS.CAMERA
    })

    let cameraPerssionStatus = null

    await Promise.all([
        check(cameraPermission)
    ]).then(([cameraStatus]) => {
        cameraPerssionStatus = cameraStatus
    })

    if (cameraPerssionStatus === RESULTS.DENIED) {
        await Promise.all([
            request(cameraPermission)
        ]).then(([cameraStatus]) => {
            console.log(cameraStatus)
        })
    } else if (cameraPerssionStatus === RESULTS.GRANTED) {
        
        const options = {
            chooseWhichLibraryTitle: null,
            tintColor: colors.primary
        }

        ReactImagePicker.launchCamera(options, (response) => {
            result(response)
        });
    }
}

export async function getPhotoFromGalery(result) {

    let imageURI = null

    let galleryPermission = Platform.select({
        android: PERMISSIONS.ANDROID.READ_EXTERNAL_STORAGE,
        ios: PERMISSIONS.IOS.MICROPHONE
    })

    let mediaPermission = Platform.select({
        android: PERMISSIONS.ANDROID.WRITE_EXTERNAL_STORAGE,
        ios: PERMISSIONS.IOS.PHOTO_LIBRARY
    })

    let galleryPermissionStatus = null
    let mediaPermissionStatus = null

    await Promise.all([
        check(galleryPermission),
        check(mediaPermission)
    ]).then(([galleryStatus, mediaStatus ]) => {
        galleryPermissionStatus = galleryStatus
        mediaPermissionStatus = mediaStatus
    })

    if (galleryPermissionStatus === RESULTS.DENIED || mediaPermissionStatus === RESULTS.DENIED) {
        await Promise.all([
            request(galleryPermission),
            request(mediaPermission)
        ]).then(([galleryStatus, mediaStatus ]) => {
      
        })
    } else if (galleryPermissionStatus === RESULTS.GRANTED && mediaPermissionStatus === RESULTS.GRANTED) {
        
        const options = {
            chooseWhichLibraryTitle: null,
            tintColor: colors.primary
        }

        ReactImagePicker.launchImageLibrary(options, (response) => {
            result(response)
        }); 
    }
 
    return imageURI  
}