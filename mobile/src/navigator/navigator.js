import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    SafeAreaView,
    Text
} from 'react-native';

import { createStackNavigator, HeaderTitle } from 'react-navigation-stack'
import { createMaterialBottomTabNavigator } from 'react-navigation-material-bottom-tabs';
import { createAppContainer } from 'react-navigation'

import LoginScreen from '../modules/login/LoginScreen'
import RegisterScreen from '../modules/register/RegisterScreen'
import FeedScreen from '../modules/feed/FeedScreen'
import SearchScreen from '../modules/search/SearchScreen'
import NotificationScreen from '../modules/notification/NotificationScreen'
import ProfileScreen from '../modules/profile/ProfileScreen'

import colors from '../../resources/values/colors.js'

import ICHouse from '../../resources/icons/ICHouse.js'
import ICMagnifier from '../../resources/icons/ICMagnifier.js'
import ICNotification from '../../resources/icons/ICNotification.js'
import ICPeople from '../../resources/icons/ICPeople.js'

export const ClosedRoutes = createMaterialBottomTabNavigator(
    {
        Feed: {
            screen: FeedScreen,
            navigationOptions: {                
                tabBarIcon: ({ focused }) => <ICHouse width={24} height={24} fill={focused ? colors.bottomBarActive : colors.bottomBarInactive}/>,
            }
        },
        Search: {
            screen: SearchScreen,
            navigationOptions: {
                tabBarIcon: ({ focused }) => <ICMagnifier width={24} height={24} fill={focused ? colors.bottomBarActive : colors.bottomBarInactive}/>,
            }
        },
        Notifications: {
            screen: NotificationScreen,            
            navigationOptions: {
                tabBarIcon: ({ focused }) => <ICNotification width={24} height={24} fill={focused ? colors.bottomBarActive : colors.bottomBarInactive}/>,
            }
        },
        Profile: {
            screen: ProfileScreen,
            navigationOptions: {
                tabBarIcon: ({ focused }) => <ICPeople width={24} height={24} fill={focused ? colors.bottomBarActive : colors.bottomBarInactive}/>,
            }
        },
    },
    {
        initialRouteName: 'Feed',
        labeled: false,
        activeColor: colors.bottomBarActive,
        inactiveColor: colors.bottomBarInactive,
        barStyle: { 
            backgroundColor: colors.bottomBarBackground,
            paddingBottom: 10,
         },

    }
)

export const OpenRoutes = createAppContainer(
    createStackNavigator({
        Login: {
            screen: LoginScreen,
            navigationOptions: {
                headerShown: false,
                headerStyle: {
                    backgroundColor: colors.primary
                }
            }
        },
        Register: {
            screen: RegisterScreen,
            navigationOptions: {
                headerTitleAlign: 'center',
                headerTitle: 'Cadastro',
                headerTintColor: '#FFF',
                headerTitleAllowFontScaling: true,
                headerBackTitleVisible: false,
                headerStyle: {
                    backgroundColor: colors.primary
                }
            }
        },
        App: {
            screen: ClosedRoutes,
            navigationOptions: {
                headerShown: false,
                headerStyle: {
                    backgroundColor: colors.primary
                }
            }
        }
    })
)