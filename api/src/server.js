const express = require('express');
const mongoose = require('mongoose');
const routes = require('./routes');
const path = require('path')
const cors = require('cors')
const app = express();

// configura o banco
mongoose.connect('mongodb://localhost:27017/parrot', {
    useNewUrlParser: true,
    useUnifiedTopology: true,
});

// configuracoes do app
app.use('/files', express.static(path.resolve(__dirname, '..', 'files')));
app.use(cors())
app.use(express.json())
app.use(routes);

// yarn dev
app .listen(3000)